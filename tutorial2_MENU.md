# The Agent Protocol

A primary goal of the TWC Project is to explore modular compositors,
including the development of APIs allowing modules to serve as an
extension of a base compositor.  One such API, deployed as a Wayland
protocol extension, is the [Agent
Protocol](https://gitlab.com/twc_project/agent_protocol "Agent
Protocol").  It can be thought of as a window manager API.  See the
[TWC_Project](https://gitlab.com/twc_project "TWC_Project") for more
context.

The Agent Protocol uses protocol objects, called *agents*, to provide
(graphical) user interfaces for the purpose of dynamic compositor
control.  Agents solicit user input and relay user intentions to the
compositor.  Without agents, a modular compositor may have few
features and no graphical personality.

To underscore the point, we present a screenshot of a bare-bones
compositor with no extensions.  It is the "most boring screenshot
ever."

![](./img/boring.png "boring screenshot")

In the above screenshot, the only visible artifact is the blue
background.  Anything else must be provided by some client including
those serving as agents.

## Menu Agent

One of the agents in the Agent Protocol is the Menu Agent.  It is
defined as a specific interface object called *xx_wm_menu_agent*.
This interface may be bound by at most one client.  We call such a
client a menu client or a menu agent.

As the name implies, a menu agent is intended to provide a generic way
to present the user with many options, e.g. circle_up[^1].  For this
reason, the menu agent interface has the most requests of all agents.

But it also provides a more basic function as can be seen in the
following screenshot of a modular compositor with a menu agent.

![](./img/menu_cursor.png "menu cursor")

The above screenshot may appear to be "yet another boring screenshot,"
but there's actually a lot going on.

Although you can't tell, the small image you see in the center of the
screen is a cursor image.  It was provided by a menu agent.

There may only be one menu agent at a time, but there may be many
different menu capable clients available for a given compositor to
employ.  This one happens to choose a tiny Wayland logo as a cursor
image.  Other implementations may choose differently, including
supplying no cursor image at all.

The point is that the cursor image is completely up to the operating
menu agent.  It is not baked into the bare-bones compositor.  This
type of behavior is generally true for every agent.  All visible
artifacts used by a modular compositor are actually provided by
agents.  A given compositor may be configured to deploy any collection
of agent implementations in any mix-and-match combination.  The
compositor look and feel is up to the agents.

Now let's get to the real interesting part.  Since any client may
change the cursor image after acquiring the pointer, how does the menu
agent know when to supply a cursor?  The answer is an Agent Protocol
concept called the Default Surface.

Any compositor which supports the Agent Protocol must include a
pre-defined surface called the Default Surface.  It exists by fiat.
The menu agent may *annex* the Default Surface and acquire a reference
as follows:

```
    struct wl_surface *default_surface = (
        xx_wm_menu_agent_annex_default_surface(
            xx_wm_menu_agent
        )
    );
```

The Default Surface covers the entire layout including all outputs.
It occupies all workspaces.  It cannot be changed or destroyed.  It is
completely transparent.  And, its most salient feature, it always
stacks on the bottom.

Every request of the wl_surface interface involving a reference to the
Default Surface is either a no-op or a protocol error.  However, other
behavior involving the Default Surface is still applicable.  For
example, the wl_pointer_set_cursor() request applies if the pointer
focus lies within the (annexed) Default Surface.  This is how the menu
agent above was able to display a cursor image.


Despite the restrictions, the Default Surface can participate in
interactions between the compositor and the menu client.  In
particular, the following events may involve the Default Surface[^2].

```
    wl_pointer::enter
    wl_pointer::leave
    wl_pointer::motion
    wl_pointer::button

    wl_keyboard::enter
    wl_keyboard::leave
    wl_keyboard::key
    wl_keyboard::modifiers
```

We can now see how a menu agent knows when to set the cursor.  That
is, upon receiving a pointer enter event for the Default Surface.

The Default Surface can be though of as the surface of last resort.
When the pointer is not located in any client surface (stacked
higher), the pointer is located in the Default Surface.  In this case,
pointer button events are delivered to the menu agent.  The Default
Surface may also acquire keyboard focus, e.g., FocusFollowsPointer.
In this case, keyboard events will also be delivered to the menu
agent.  These input events are known as *orphan input*.

Recall that the menu agent is intended to expose the user to many
options.  This is the reason why orphan input belongs to the menu
agent.  It can, at its discretion, associate any combination of key or
button events with a wide range of available control options.  These
can be relayed to the compositor without need for visual interaction.
The menu agent may, but is not required to, use graphical imaging to
solicit user input.

The menu agent we've been following does use imaging for input.  In
particular, it has a simple menu associated with a right button click
within the Default Surface.

![](./img/menu.png "simple menu")

The above screenshot shows the response of our menu agent to a right
button click.  Once again, there is a lot going on.

This menu agent presents a toplevel surface with a red background and
a single-item list of menu entries.  This single entry happens to
launch a terminal emulator.  Note that the the agent has chosen to
change the cursor as the pointer is located within one of its
surfaces.

None of this imagery is from the compositor.  It is all generated by
the menu agent.  The agent gets to decide on the size and content of
the toplevel.  It doesn't have to resemble a menu.  It could be a
dialog box where the user types in a "command" from some custom "menu
language."  If the user doesn't like a particular menu client, they
can direct the compositor to use a different one.  Agents themselves
can be independently configured, providing another avenue for the user
to influence agent behavior/appearance.

The obvious question is why wasn't this menu toplevel visible before.

One possibility is that the menu agent created a new toplevel in
response to the right button click.  Since the content of agent
windows may be complex, some agents may choose to prepare their
toplevels in advance.  This is the case for our demonstration menu
agent.  It has chosen to use a concept called *Desktop Services* to
give the compositor/user control over how some of its toplevels are
managed.

## Desktop Services

Agents may create named services.  For example, the workspace agent
may provide a "Workspace Switcher" service.  In our example, the menu
agent uses the string "MainMenu" as the name of the service visible
above.  Using menu agent configuration, the user has defined the
MainMenu, including its content, affinity to the right button and even
the name itself.

In general agents may create named services.  This results in a
desktop_service interface.  Using the interface, the agent will
associate a toplevel with the service.  In addition, the compositor
delivers a new_service event to the menu agent.  The menu agent will
bind a service_controller object for future use.  Later the menu agent
can use the call_for_service request of the service controller to
*assert* the corresponding service.

For example, as part of menu agent configuration, the user may assign
the "Workspace Switcher" service to some orphan input.  When seen, the
menu agent will assert the service_controller corresponding to the
switcher service.

In our running case above, the service happens to belong to the menu
agent itself, but in general, any agent may name their services.
However, only the menu agent and compositor can make use of service
controllers.

A service assertion is a directive to the compositor to make the
toplevel corresponding to a service "conspicuous" to the user.  The
manner in which this is accomplished is meant to be flexible.  It is
up to the compositor to achieve.  However, there is a defined set of
variations for "make conspicuous."  They are listed below.

  - no-op
  - deiconify
  - raise to top
  - relocate

In our example, the menu agent receives a right button click.  It
responds by asserting the service_controller for the MainMenu service
that we see.  The menu agent has no idea what the compositor will do,
only that the user is interested in a particular menu.  So it notifies
the compositor through the assertion.

Continuing our example, we see that the MainMenu has been configured,
using its service name, to deiconify, raise and relocate upon
assertion.  This configuration may be a default or explicitly written
by the user.  For relocate, the compositor makes an effort to place
the surface near the pointer.  As a convenience, a service toplevel
may also be given a hotspot.  In any event, the details for providing
(conspicuous) access to agent services is mostly in the hands of the
agent and user, not the compositor.

Service assertions are essentially a fancy popup mechanism.  So why
can't it be done with xdg_popups?  First, xdg_popups require a parent
surface.  Second, one client can't popup the surfaces of another
client.  Lastly, popups are generally positioned relative to an
existing surface and aren't subject to compositor relocation.

One more thing about our menu agent and its single-item menu.  Any
compositor may support the ability to iconify a window.  In our
running example, the user has configured the MainMenu service to start
in the iconify state and to have the property of iconify-by-masking,
i.e., hidden.  That is why there is no trace of the menu before it is
asserted.

So how does the menu return to a non-asserted state?  The answer is
that assertion has a companion operation called *stand_down*.  Using
the desktop_service interface, an agent may make a stand_down request
for any of its named services.  In the case of a menu, the menu agent
will probably issue a stand down request just after the user makes a
selection.  Alternatively, an agent may stand down a service just
after the user clicks on an "apply" tab.

A service is asserted due to the user expressing an immediate need.
In general, an agent determines when the users immediate need is
satisfied and then issues the stand down.  As with assertion, the
agent has no idea of what the compositor will do.  The agent is merely
expressing the that user is done for now.

It should be no surprise that the method of stand down is meant to be
flexible.  The possibilities are defined as shown below.

  - no-op
  - lower
  - iconify

As before, the behavior of stand down is not determined by the
compositor.  The deployed agents and the user have a lot to say about
how the assert/stand_down cycle works.  The sequence of actions can be
individually controlled for each agent service.  In our example, the
user has configured the stand down method for the MainMenu to be
iconify.  Since that menu is iconify-by-masking, it simply disappears
from view.

[^1]: The compositor operation circle_up pushes the top window to the
      stack bottom while all others move up.

[^2]: With regard to wl_surface::enter, the menu agent may assume that
      the Default Surface has entered every output and will never
      leave.  The buffer_scale and buffer_transform are moot.  For
      this reason, wl_surface_add_listener() can be a no-op.  Events
      for the Default Surface will never be emitted.

[^3]: To avoid conflicts, each tag logically has an integer and an
      agent component.  In the example, the workspace manual
      identifies which tag corresponds to reoccupy.  The user can then
      configure a decoration widget to use this tag.
